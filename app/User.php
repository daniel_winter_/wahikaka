<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';
    public $primaryKey = 'id';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $rules = [
        'update' => [
            'name'              => 'required',
            'email'             => 'required'
        ]
    ];

    public static $messages  = [
         'update' => [
            'name.required'                 => 'Bitte geben Sie einen Namen an',
            'email.required'                => 'Bitte geben Sie eine Email an'
         ]
    ];
}
