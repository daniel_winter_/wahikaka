<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wahi extends Model
{
    protected $table = 'wahis';
    public $primaryKey = 'id';
}
