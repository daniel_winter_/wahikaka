<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Wahi;
use Auth;
use Validator;
use Input;
use Session;
use Redirect;
use DB;
use Paginator;
use Carbon\Carbon;
use View;
use Response;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;

class WahiController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $allTasks = Wahi::find($id);
        // Object
        //return $allTasks;
        // Array
        //return Response::json(array($allTasks));

        $wahis = Wahi::find($id);

        $wahiID = $id;

        $wahiToCity = DB::table('wahis as w')
            ->select('w.toCity')
            ->where('w.id', '=', $wahiID)
            ->pluck('w.toCity');

        $wahiLatTo = DB::table('wahis as w')
            ->select('w.latTo')
            ->where('w.id', '=', $wahiID)
            ->pluck('w.latTo');

        $wahiLonTo = DB::table('wahis as w')
            ->select('w.lonTo')
            ->where('w.id', '=', $wahiID)
            ->pluck('w.lonTo');

        $wahiLatFrom = DB::table('wahis as w')
            ->select('w.latFrom')
            ->where('w.id', '=', $wahiID)
            ->pluck('w.latFrom');

        $wahiLonFrom = DB::table('wahis as w')
            ->select('w.lonFrom')
            ->where('w.id', '=', $wahiID)
            ->pluck('w.lonFrom');
        
        $wahiToCity = str_replace(array('[', ']'), '', htmlspecialchars(json_encode($wahiToCity), ENT_NOQUOTES));

        $wahiLatTo = str_replace(array('[', ']'), '', htmlspecialchars(json_encode($wahiLatTo), ENT_NOQUOTES));
        $wahiLonTo = str_replace(array('[', ']'), '', htmlspecialchars(json_encode($wahiLonTo), ENT_NOQUOTES));
        $wahiLatFrom = str_replace(array('[', ']'), '', htmlspecialchars(json_encode($wahiLatFrom), ENT_NOQUOTES));
        $wahiLonFrom = str_replace(array('[', ']'), '', htmlspecialchars(json_encode($wahiLonFrom), ENT_NOQUOTES));


        Mapper::map($wahiLatTo, $wahiLonTo, ['zoom' => 10])
            ->polyline([['latitude' => $wahiLatFrom, 'longitude' => $wahiLonFrom],
                        ['latitude' => $wahiLatTo, 'longitude' => $wahiLonTo]],
                        ['strokeColor' => '#000000', 'strokeOpacity' => 0.35, 'strokeWeight' => 5]);
        
        return view('wahis.show', ['wahis' => $wahis]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
