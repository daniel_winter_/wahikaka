<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Wahi;
use Auth;
use Validator;
use Input;
use Session;
use Redirect;
use DB;
use Paginator;
use Carbon\Carbon;
use View;
use Response;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;


class HomeController extends WahiController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth::user()->id;
        $wahis = DB::table('wahis as w')
            ->join('users as u', 'w.userid', '=', 'u.id')
            ->where('w.userid', '=', $userid)
            ->select('w.id', 'w.steps', 'w.fromCity', 'w.toCity', 'w.latTo', 'w.lonTo', 'w.toCountry', 'w.direction', 'w.infoText')
            ->orderBy('w.steps', 'desc')
            ->paginate(10);

        $wahiLatLon = DB::table('wahis as w')
            ->join('users as u', 'w.userid', '=', 'u.id')
            ->where('w.userid', '=', $userid)
            ->select('w.latTo', 'w.lonTo', 'w.fromCity', 'w.toCity')
            ->get();

        return view('home', ['wahis' => $wahis, 'wahiLatLon' => $wahiLatLon]);
    }
}
