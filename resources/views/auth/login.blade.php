<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <!-- <label for="email" class="control-label">E-Mail Address</label> -->

        <div class="col-md-12">
            <input id="email" type="email" placeholder="Your email address" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <!-- <label for="password" class="control-label">Password</label> -->

        <div class="col-md-12">
            <input id="password" type="password" placeholder="********" class="form-control" name="password" required>

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember"> Remember Me
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <button type="submit" class="btn btn-wahi btn-block">
                Login
            </button>

            <a href="{{ url('/register') }}" class="btn-register">Noch keinen Account? Register dich!</a>

<!--                     <a class="btn btn-link" href="{{ url('/password/reset') }}">
                Forgot Your Password?
            </a> -->
        </div>
    </div>
</form>
