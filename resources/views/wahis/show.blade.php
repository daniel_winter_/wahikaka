@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            
            <a href="{{ URL::to('/home') }}" class="btn btn-wahi pull-right">Zurück zur Übersicht</a>

			<h1>Detailübersicht</h1>

            <div class="content__detailview">
                <div class="content__detailview-map">{!! Mapper::render() !!}</div>
            
                <div class="content__detailview-info">
                    <p><i class="fa fa-hand-o-right fa-fw fa-2x" aria-hidden="true"></i> {{ $wahis->steps }} Schritte, in Richtung: {{ $wahis->direction }}</p>
                    <p><i class="fa fa-map-marker fa-fw fa-2x" aria-hidden="true"></i> von: {{ $wahis->fromCity }} - nach {{ $wahis->toCity }}, {{ $wahis->toCountry }}</p>
                    <p><i class="fa fa-quote-left fa-fw fa-2x" aria-hidden="true"></i>{{ $wahis->infoText }}</p>
                </div>
            </div>

    	</div>
    </div>
</div>
@endsection

