<h1>Meine Wahikakas</h1>

<h4>Schau dir an wohin du schon gegangen bist!</h4>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true&key=AIzaSyAK6mYhbwDDfsKBx5V_ywq8Nb8gCOggv5c"></script>
<script type="text/javascript" src="js/gmaps.js"></script>
<script type="text/javascript">
    var map;
    $(document).ready(function(){
        map = new GMaps({
            el: '#map',
            zoom: 7,
            lat: 48.86648132834658,
            lng: 9.193903442964318
        });

        @foreach($wahiLatLon as $latlon)
            map.addMarker({
                lat: {{ $latlon->latTo }},
                lng: {{ $latlon->lonTo }},
                title: "{{ $latlon->toCity }}",
                infoWindow: {
                  content: '<p style="color: black;">Start: {{ $latlon->fromCity }} <br/> Ziel: {{ $latlon->toCity }}</p>'
                },
            });
        @endforeach
    });
</script>
<div class="content__detailview">
    <div class="content__detailview-map" id="map" style="height: 300px;"></div>
</div>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
            	<th></th>
                <th>Datum</th>
                <th>Schritte</th>
                <th>von (Stadt)</th>
                <th>bis (Stadt)</th>
                <th>bis (Land)</th>
                <th>Richtung</th>
                <th>Text</th>
            </tr>
        </thead>
        @foreach($wahis as $wahi)
            <tr>
				<td><a class="btn btn-wahi btn-sm" href="{{ URL::to('wahi/' . $wahi->id . '') }}">Show Details</a></td>
                <td> 23.12.2016</td>
                <td>{{ $wahi->steps }}</td>
                <td>{{ $wahi->fromCity }}</td>
                <td>{{ $wahi->toCity }}</td>
                <td>{{ $wahi->toCountry }}</td>
                <td>{{ $wahi->direction }}</td>
                <td>{{ $wahi->infoText }}</td>
                </td>
            </tr>
        @endforeach
    </table>
    <?php echo $wahis->render(); ?>
</div>