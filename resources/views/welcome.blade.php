@include('includes.header')
<div class="wrapper__home">
    <div class="wrapper__overlay">

        <div class="wrapper__logo">
            <div class="wrapper__logo-intro">
                <h1>Wahikaka</h1>
                <h3>Entdecke neue Orte</h3>
                    @if (Route::has('login'))
                        @if (Auth::check())
                            <a href="{{ url('/home') }}" class="btn btn-wahi btn-block">Zu meinen Wahikakas</a>
                        @else
                            @include('auth.login')
                        @endif
                    @endif
            </div>
        </div>
    </div>
</div>
@include('includes.footer')
