@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
		    <h1>Bearbeite Profil {{ $user->name }}</h1>

	        @if($errors->any())
		      <ul class="alert alert-danger">
		        @foreach($errors->all() as $error)
		          <li>{{$error}}</li>
		        @endforeach
		      </ul>
		    @endif

		    {{ Form::model($user, array('route' => array('user.update', $user->id), 'method' => 'PUT')) }}

		        {{ Form::hidden('id', $user->id) }}

		            <div class="form-group">
		              {{ Form::label('name', 'Name*') }}
		              {{ Form::text('name', null, array('class' => 'form-control')) }}
		            </div>

		            <div class="form-group">
		              {{ Form::label('email', 'Email*') }}
		              {{ Form::text('email', null, array('class' => 'form-control')) }}
		            </div>

		        <p>
		            {{ Form::submit('Speichern!', array('class' => 'btn btn-wahi')) }}
		            <a href="{{ URL::to('/user') }}" class="btn btn-danger">Abbrechen</a>
		        </p>

		    {{ Form::close() }}

        </div>
    </div>
</div>

@stop