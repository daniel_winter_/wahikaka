@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <h1>Mein Profil</h1>

            @if( Session::has('updateUser') )
                <p class="alert alert-success"> <i class="fa fa-check" aria-hidden="true"></i> Profil erfolgreich updated</p>
            @endif

            <div class="row">
                @foreach($user as $userData)
                <p class="col-md-2"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Name: </p>
                <p class="col-md-10">{{ $userData->name }}</p>

                <p class="col-md-2"><i class="fa fa-envelope-o" aria-hidden="true"></i> Email: </p>
                <p class="col-md-10">{{ $userData->email }}</p>

                <p class="col-md-2"><i class="fa fa-eye-slash" aria-hidden="true"></i></i> Passwort: </p>
                <p class="col-md-10">********</p>

                @endforeach
                <p class="col-md-4">
                	<a class="btn btn-wahi btn-block" href="{{ URL::to('user/' . $userData->id . '/edit') }}"><i class="fa fa-pencil" aria-hidden="true"></i> Profil verwalten</a></td>
				</p>
            </div>
        </div>
    </div>
</div>
@endsection